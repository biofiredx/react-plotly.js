"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _factory = _interopRequireDefault(require("./factory"));

var _plotly = _interopRequireDefault(require("plotly.js-cartesian-dist"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PlotComponent = (0, _factory.default)(_plotly.default);
var _default = PlotComponent;
exports.default = _default;
//# sourceMappingURL=react-plotly.js.map